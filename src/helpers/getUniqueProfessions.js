const getUniqueProfessions = (totalData) => {
    let professionArray = []
    totalData.map(gnome => {
        gnome.professions.map(profession => {
            if (professionArray.indexOf(profession) === -1) {
                professionArray.push(profession)
            }
        })
    })
    return(professionArray)
}

export default getUniqueProfessions