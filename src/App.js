import React, { Component } from 'react'
import './App.css'
import { Grid, Row, Col, Button } from 'react-bootstrap'
import Main from './components/Main'
import SideBar from './components/SideBar'
import BurgerMenu from './components/BurgerMenu'
import NavBarTop from './components/NavBarTop'
import axios from 'axios'
import swal from 'sweetalert'
import getUniqueProfessions from './helpers/getUniqueProfessions'

class App extends Component {
  constructor() {
    super()

    this.state = {
      townPopulation: [],
      showSidebar: false,
      start: 0,
      numberOfResults: 10,
      searched: false,
      showModal: false,
      clickedOnModal: false,
      ageFilterValue: 0,
      heightFilterValue: 0,
      weightFilterValue: 0,
      occupationFilterValue: undefined,
      sidebarState: 'inactive'
    }
  }


  componentDidMount = async () => {
    try {
      let baseURL = 'https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json' 
      let { data } = await axios.get(baseURL)
      let townPopulation = data.Brastlewark
      let currentData = townPopulation.slice(this.state.start, this.state.numberOfResults)
      let uniqueProfessions = getUniqueProfessions(townPopulation)
      this.setState({
        townPopulation: currentData,
        totalData: townPopulation,
        uniqueProfessions: uniqueProfessions
      })
    }
    catch (err) {
      swal('Oops!...', `Something went wrong! ${JSON.stringify(err, null, 2)}`, "error")
    }
  }

  updatePagination = () => {
    this.setState({
      start: this.state.start + 20,
      numberOfResults: this.state.numberOfResults + 20
    }, () => {
      let townPopulation = this.state.totalData
      let currentData = townPopulation.slice(this.state.start, this.state.numberOfResults)
      this.setState({
        townPopulation: [...this.state.townPopulation, ...currentData]
      })
    })
  }

  updateSearchTerm = (e) => {
    this.setState({
      'searchTerm': e.target.value
    })
  }

  handleSearchSubmit = (e) => {
    e.preventDefault()
    let townPopulation = this.state.totalData
    let foundTerm = townPopulation.filter(gnome => {
      return gnome.name.includes(this.state.searchTerm)
    })
    if (foundTerm.length > 0) {
      this.setState({
        townPopulation: foundTerm,
        searched: true
      })
    }
    else {
      swal('Oops!...', `No info was found!`, "error")
    }
  }

  handleAgeRangeChange = (value) => {
    let { townPopulation } = this.state
    let ageFilteredResults = townPopulation.filter(gnome => {
      return (gnome.age === value || gnome.age > value)
    })
    this.setState({
      ageFilterValue: value,
      townPopulation: ageFilteredResults
    })
  }

  handleWeightRangeChange = (value) => {
    let { townPopulation } = this.state
    let weightFilteredResults = townPopulation.filter(gnome => {
      return (gnome.weight === value || gnome.weight > value)
    })
    this.setState({
      weightFilterValue: value,
      townPopulation: weightFilteredResults
    })
  }

  handleHeightRangeChange = (value) => {
    let { townPopulation } = this.state
    let heightFilteredResults = townPopulation.filter(gnome => {
      return (gnome.height === value || gnome.height > value)
    })
    this.setState({
      heightFilterValue: value,
      townPopulation: heightFilteredResults
    })
  }

  onClearFilters = () => {
    this.setState({
      ageFilterValue: 0,
      weightFilterValue: 0,
      searchTerm: undefined,
      townPopulation: this.state.totalData,
      clickedOnModal: false
    }, () => {
      swal("Done!", "Filters cleared", "success")
    })
  }

  findFriend = (friendName) => {
    let { totalData } = this.state
    let foundFriend = totalData.filter(gnome => {
      return gnome.name === friendName
    })[0]
    return foundFriend
  }

  handleImageClick = (e) => {
    let foundGnome = this.findFriend(e.target.id)
    this.setState({
      clickedGnome: foundGnome,
      showModal: true
    })
  }

  handleFriendClick = (e) => {
    let friends = e.target.getAttribute('value').split(',')
    let foundFriends = friends.map(this.findFriend)
    this.setState({
      townPopulation: foundFriends,
      showModal: false,
      clickedOnModal: true,
      clickedOnModalType: 'friends',
      clickedOnModalValue: this.state.clickedGnome.name
    })
  }

  handleProfessionClick = (e) => {
    let { totalData } = this.state
    let foundWorkers = totalData.filter(gnome => {
      return gnome.professions.includes(e.target.getAttribute('value'))
    })
    this.setState({
      townPopulation: foundWorkers,
      showModal: false,
      clickedOnModal: true,
      clickedOnModalType: 'profession',
      clickedOnModalValue: e.target.getAttribute('value')
    })
  }

  handleModalClose = () => {
    this.setState({
      showModal: false
    })
  }

  handleSidebarState = () => {
    if (this.state.showSidebar === false) {
      this.setState({
        showSidebar: true,
        sidebarState: 'active'
      })
    }
    else {
      this.setState({
        showSidebar: false,
        sidebarState: 'inactive'
      })
    }
  }

  render() {
    return (
      <div className="App">
        <Grid className={`container-fluid app-grid`}>
          <Row className="row-fluid">
            <Col lg={4} md={6} sm={12} className="sidebar-col">
              <div className={`sidebar-header ${this.state.sidebarState}`}>
                <div className="site-title-container">
                  <Button onClick={this.handleSidebarState} className={`burgerButton ${this.state.sidebarState}`}><i className="fas fa-bars"></i></Button>
                  <h2 className="site-title">BrastlewarkBook</h2>
                </div>
              </div>
              {
                this.state.showSidebar
                  ? <SideBar
                    handleAgeRangeChange={this.handleAgeRangeChange}
                    ageFilterValue={this.state.ageFilterValue}
                    handleWeightRangeChange={this.handleWeightRangeChange}
                    weightFilterValue={this.state.weightFilterValue}
                    handleHeightRangeChange={this.handleHeightRangeChange}
                    heightFilterValue={this.state.heightFilterValue}
                    onClearFilters={this.onClearFilters}
                    uniqueProfessions={this.state.uniqueProfessions}
                    handleProfessionClick={this.handleProfessionClick}
                  />
                  : <div></div>

              }

            </Col>
            <Col lg={8} md={6} sm={12} className={`main-col ${this.state.sidebarState}`}>
              <NavBarTop
                clickedOnModal={this.state.clickedOnModal}
                clickedOnModalType={this.state.clickedOnModalType}
                clickedOnModalValue={this.state.clickedOnModalValue}
                showModal={this.props.showModal}
                filterInfo={this.state.filterInfo}
              />
              {
                this.state.townPopulation.length > 0
                  ? <Main
                    townPopulation={this.state.townPopulation}
                    updatePagination={this.updatePagination}
                    handleImageClick={this.handleImageClick}
                    clickedGnome={this.state.clickedGnome}
                    showModal={this.state.showModal}
                    closeModal={this.handleModalClose}
                    handleFriendClick={this.handleFriendClick}
                    handleProfessionClick={this.handleProfessionClick}
                    className={`main`}
                  />
                  : <p className="noresults">Oops...no inhabitants match your query! :(</p>
              }
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App
