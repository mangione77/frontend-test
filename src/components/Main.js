import React, { Component } from 'react'
import PopulationList from './PopulationList'
import ResultModal from './ResultModal'

class Main extends Component {
    render() {
        return (
            <div className="main">
                {
                    !this.props.showModal
                        ? <PopulationList
                            townPopulation={this.props.townPopulation}
                            handleImageClick={this.props.handleImageClick}
                        />
                        : <ResultModal
                            showModal={this.props.showModal}
                            closeModal={this.props.closeModal}
                            clickedGnome={this.props.clickedGnome}
                            handleFriendClick={this.props.handleFriendClick}
                            handleProfessionClick={this.props.handleProfessionClick}
                        />
                }
            </div>
        )
    }
}

export default Main