import React, { Component } from 'react'
import { FormGroup, ControlLabel, Button } from 'react-bootstrap'
import SliderFilter from '../components/common/SliderFilter'
import uuid from 'uuid/v4'

class SideBar extends Component {
    render() {
        return (
            <div className="wrapper">
                <nav id="sidebar">
                    <ul className="list-unstyled components">
                        <li>
                            <FormGroup>
                                <ControlLabel className="form-label filters-title"><i className="fab fa-connectdevelop pull-right sidebar-icon"></i>Filters</ControlLabel><br />
                                <ControlLabel className="sidebar-controllabel">Filter by age</ControlLabel>
                                <SliderFilter
                                    min={0}
                                    max={400}
                                    defaultValue={0}
                                    value={this.props.ageFilterValue}
                                    className="range-slider"
                                    onAfterChange={this.props.handleAgeRangeChange}
                                    descriptionText={'Equal or greater than'}
                                    filterValue={this.props.ageFilterValue}
                                />
                                <ControlLabel className="sidebar-controllabel">Filter by weight</ControlLabel>
                                <SliderFilter
                                    min={0}
                                    max={60}
                                    defaultValue={0}
                                    value={this.props.weightFilterValue}
                                    className="range-slider"
                                    onAfterChange={this.props.handleWeightRangeChange}
                                    descriptionText={'Equal or greater than'}
                                    filterValue={this.props.weightFilterValue}
                                />
                                <ControlLabel className="sidebar-controllabel">Filter by height</ControlLabel>
                                <SliderFilter
                                    min={0}
                                    max={150}
                                    defaultValue={0}
                                    value={this.props.heightFilterValue}
                                    className="range-slider"
                                    onAfterChange={this.props.handleHeightRangeChange}
                                    descriptionText={'Equal or greater than'}
                                    filterValue={this.props.heightFilterValue}
                                />
                                <div className="btn-group profession-container" role="group">
                                    <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Filter by profession
                                    <span className="caret"></span>
                                    </button>
                                    <ul className="dropdown-menu">
                                        {
                                            this.props.uniqueProfessions !== undefined
                                                ? this.props.uniqueProfessions.map(profession => {
                                                    return <li key={profession}><a className="dropdown-item" value={profession} onClick={this.props.handleProfessionClick}>{profession}</a></li>
                                                })
                                                : <div></div>
                                        }
                                    </ul>
                                </div><br />
                                <Button className="clear-filters" onClick={this.props.onClearFilters}>Clear filters</Button>
                            </FormGroup>
                        </li>
                    </ul>
                </nav>
            </div >
        )
    }
}

export default SideBar