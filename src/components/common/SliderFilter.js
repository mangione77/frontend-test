import React, { Component } from 'react'
import 'rc-slider/assets/index.css';
import Slider from 'rc-slider'
import Tooltip from 'rc-tooltip'
const Handle = Slider.Handle

class SliderFilter extends Component {
    handle = (props) => {
        const { value, dragging, index, ...restProps } = props;
        return (
            <Tooltip
                prefixCls="rc-slider-tooltip"
                overlay={value}
                visible={dragging}
                placement="top"
                key={index}
            >
                <Handle value={value} {...restProps} />
            </Tooltip>
        )
    }
    render() {
        return (
            <div>
                <Slider
                    min={this.props.min}
                    max={this.props.max}
                    defaultValue={this.props.defaultValue}
                    handle={this.handle}
                    className={this.props.className}
                    onAfterChange={this.props.onAfterChange}
                />
                {
                    !this.props.filterValue
                    ? <div></div>
                    : <p className="range-description">{this.props.descriptionText}: {this.props.filterValue}</p>                    
                }
            </div>
        )
    }
}

export default SliderFilter