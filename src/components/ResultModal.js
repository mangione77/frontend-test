import React, { Component } from 'react'
import { Modal, Image, Button } from 'react-bootstrap'
import uuid from 'uuid/v4'

class ResultModal extends Component {
    render() {
        return (
            <Modal show={this.props.showModal} onHide={this.props.closeModal} animation={false} className="gnome-modal">
                <Modal.Header>
                    <Modal.Title>{this.props.clickedGnome.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Image
                        className="gnome-avatar image"
                        src={this.props.clickedGnome.thumbnail}
                        alt={`${this.props.clickedGnome.name}'s avatar`}
                        value={this.props.clickedGnome.name}
                        rounded
                        id={this.props.clickedGnome.name}
                    />
                    <p>Age: {this.props.clickedGnome.age}</p>
                    <p>Height: {this.props.clickedGnome.height.toFixed(2)} cm</p>
                    <p>Weight: {this.props.clickedGnome.weight.toFixed(2)} kg's</p>
                    {
                        this.props.clickedGnome.friends.length > 0
                            ? <Button onClick={this.props.handleFriendClick} value={this.props.clickedGnome.friends}>See friends!</Button>
                            : <p key={uuid()}>No friends found :(</p>
                    }
                    <div className="btn-group" role="group">
                        <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Professions
                                <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu">
                            {
                                this.props.clickedGnome.professions.length > 0
                                    ? this.props.clickedGnome.professions.map(profession => {
                                        return <li key={profession}><a className="dropdown-item" value={profession} onClick={this.props.handleProfessionClick}>{profession}</a></li>
                                    })
                                    : <li key={uuid()}>No profession found :(</li>
                            }
                        </ul>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.closeModal}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default ResultModal