import React, { Component } from 'react'
import { Grid, Row, Col, Image, Button } from 'react-bootstrap'
import uuid from 'uuid/v4'

class PopulationList extends Component {
    render() {
        return (
            <Grid className="container-fluid">
                <Row className="row-fluid">
                    {
                        this.props.townPopulation.map(gnome => {
                            return (
                                <Col lg={4} md={6} sm={12} key={uuid()}>
                                    <div className="gnome-card" onClick={this.props.handleImageClick}>
                                        <Image
                                            className="gnome-avatar image"
                                            src={gnome.thumbnail}
                                            alt={`${gnome.name}'s avatar`}
                                            value={gnome.name}
                                            rounded
                                            id={gnome.name}
                                        />
                                        <h2 className="gnome-name">
                                            <span>{gnome.name}</span>
                                            <span>{gnome.age} y/o</span>
                                        </h2>
                                    </div>
                                </Col>
                            )
                        })
                    }
                    <Col lg={12} sm={12} md={12} className="loadmore-container">
                        {
                            this.props.townPopulation.length < 10
                                ? <div></div>
                                : <Button onClick={this.props.updatePagination}>Load more!</Button>
                        }
                    </Col>
                </Row>
            </Grid>
        )
    }
}

export default PopulationList