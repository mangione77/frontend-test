import React, { Component } from 'react'
import { Navbar, FormGroup, Button } from 'react-bootstrap'


class NavBarTop extends Component {
    render() {
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                    {
                        (this.props.clickedOnModal && !this.props.showModal)
                        ? <p className="navbartop-results">Showing results for '{this.props.clickedOnModalType}': {this.props.clickedOnModalValue}</p>
                        : <div></div>
                    }
                    </Navbar.Brand>
                </Navbar.Header>

            </Navbar>
        )
    }
}

export default NavBarTop