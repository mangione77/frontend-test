import React, { Component } from 'react'
import { slide as Menu } from 'react-burger-menu'
import { FormGroup, ControlLabel, Button } from 'react-bootstrap'
import SliderFilter from '../components/common/SliderFilter'
import uuid from 'uuid/v4'

class BurgerMenu extends Component {
    constructor() {
        super()

        this.state = {
            isMenuOpen: false
        }
    }

    openMenu = () => {
        this.setState({
            isMenuOpen: true
        })
    }

    render() {
        return (
            <div>
                <Menu width={'280px'} onStateChange={this.isMenuOpen}>
                    <div className="sidebar-header menu-item">
                        <div className="site-title-container menu-item">
                            <h2 className="site-title">BrastlewarkBook</h2>
                        </div>
                        <ul className="list-unstyled components">
                            <li className="menu-item">
                                <FormGroup>
                                    <ControlLabel className="form-label"><i className="fab fa-connectdevelop pull-right sidebar-icon"></i>Filters</ControlLabel><br />
                                    <ControlLabel>Filter by age</ControlLabel>
                                    <SliderFilter
                                        min={0}
                                        max={400}
                                        defaultValue={0}
                                        value={this.props.ageFilterValue}
                                        className="range-slider"
                                        onAfterChange={this.props.handleAgeRangeChange}
                                        descriptionText={'Equal or greater than'}
                                        filterValue={this.props.ageFilterValue}
                                    />
                                    <ControlLabel>Filter by weight</ControlLabel>
                                    <SliderFilter
                                        min={0}
                                        max={60}
                                        defaultValue={0}
                                        value={this.props.weightFilterValue}
                                        className="range-slider"
                                        onAfterChange={this.props.handleWeightRangeChange}
                                        descriptionText={'Equal or greater than'}
                                        filterValue={this.props.weightFilterValue}
                                    />
                                    <ControlLabel>Filter by height</ControlLabel>
                                    <SliderFilter
                                        min={0}
                                        max={150}
                                        defaultValue={0}
                                        value={this.props.heightFilterValue}
                                        className="range-slider"
                                        onAfterChange={this.props.handleHeightRangeChange}
                                        descriptionText={'Equal or greater than'}
                                        filterValue={this.props.heightFilterValue}
                                    />
                                    <div className="btn-group profession-container" role="group">
                                        <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Filter by profession
                                    <span className="caret"></span>
                                        </button>
                                        <ul className="dropdown-menu">
                                            {
                                                this.props.uniqueProfessions !== undefined
                                                    ? this.props.uniqueProfessions.map(profession => {
                                                        return <li key={profession}><a className="dropdown-item" value={profession} onClick={this.props.handleProfessionClick}>{profession}</a></li>
                                                    })
                                                    : <div></div>
                                            }
                                        </ul>
                                    </div><br />
                                    <Button className="clear-filters" onClick={this.props.onClearFilters}>Clear filters</Button>
                                </FormGroup>
                            </li>
                        </ul>
                    </div>
                </Menu>
            </div>

        )
    }
}

export default BurgerMenu