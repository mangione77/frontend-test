# BrastlewarkBook

> 	[...] Every	time	the	heroes	they	play	arrive	at	a	town,	they	have	the	issue	they	don't	know	the	local	population	and	what	they	can	do	to	help	them	on	their	adventures.			This	is	one	of	these	times,	our	heroes	just	arrived	at	a	Gnome	town	called	Brastlewark.	To	facilitate	trade	with	the	local	population	they	need	an	easy	way	to	browse	all	the	inhabitants	details.	

BrastlewarkBook has all the info about the Brastlewark gnome population!

## [Live Demo](http://rambunctious-chin.surge.sh/)

![BrastlewarkBook](https://preview.ibb.co/mzXcxU/brastlewark_Book.png)

